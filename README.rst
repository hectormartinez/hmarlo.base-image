Docker hmarlo/base-image
========================

.. image:: https://gitlab.com/hectormartinez/hmarlo.base-image/badges/master/pipeline.svg
   :target: https://gitlab.com/hectormartinez/hmarlo.base-image/commits/master
   :alt: pipeline status

This image a Docker image prepared to run multiple services using `Supervisor <>`_.
You only need to copy your supervisor configuration files to
:code:/etc/supervisor/conf.d/` and they will be readed by supervisor and launched
on startup (if needed). The image also includes Python 3.x, since Supervisor is built
on top of Python.
