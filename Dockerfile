FROM ubuntu:18.04

WORKDIR /build/
ENV DEBIAN_FRONTEND=noninteractive LC_ALL=C

ADD . /build/

RUN set -xe \
    \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        apt-utils \
        curl \
        openssl \
        ca-certificates \
        locales \
        vim-tiny \
        python3-minimal \
        python3-distutils \
    && curl -s https://bootstrap.pypa.io/get-pip.py | python3 - \
    && python3 -m pip install https://github.com/Supervisor/supervisor/archive/master.zip --no-cache-dir \
    && mkdir -p /var/log/supervisor /etc/supervisor/conf.d/ \
    && cp /build/supervisord.conf /etc/supervisor/supervisord.conf \
    \
    && apt-get -qy autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    \
    && ln -s /usr/bin/vim.tiny /usr/bin/vim \
    && locale-gen

ENTRYPOINT ["supervisord"]
